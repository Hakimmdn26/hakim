import { View, Text, SafeAreaView, TouchableOpacity, TextInput, ImageBackground, StyleSheet } from 'react-native'
import React, { useState } from 'react'

const App = () => {

  const [nama, setnama] = useState('') // untuk ambil dan menyimpan data
  const ambil = () => {//variabel sekaligus function untuk ambil data.

    var myHeaders = new Headers(); // header untuk ngirim data ke back end
    myHeaders.append("Content-Type", "application/json");
    
    var raw = JSON.stringify({
      "name": nama// manggil dari variabel usestate
    });
    
    var requestOptions = { // request opstion
      method: 'PATCH',//GET, POST, DELETE, PATCH, PUT, OPTION
      headers: myHeaders,// untuk kirim data ke back-end
      body: raw,//isi data yang akan dikirim
      redirect: 'follow'
    };
    
    fetch( // sebuah function yang ada dari javasricpt untuk ambil data API atau data back-end
      "https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/users/-NMalhNh5LPYMCq7uuUh.json", requestOptions)
      // blob
      .then(response => //ketika API sudah dijalan maka ambil datanya
        response.text()) // kita convert dulu
      .then(result => {
        // const val = JSON.parse(result);
        // setnama(val)
        console.log(result);
        setnama(result)
      })// isi yg sudah di convert dari text
      .catch(error => console.log('error', error));  // untuk menangkap data yang error
    }
  return (
    <ImageBackground
      source={{uri:'https://t4.ftcdn.net/jpg/05/00/42/73/360_F_500427354_M5hroAGQF6uTsRrAJV29c9bFaclzv12I.jpg'}}
      style={{flex:1, justifyContent:'center'}}>
    <SafeAreaView>
      <View >
      <TextInput 
        placeholder='Ketikan Nama'// buat kolom text input
        onChangeText={nama => {//ketika input text ke kolom input, text tamplate akan berubah
          setnama(nama)// print inputan text
        }}
        style={{
          backgroundColor:'white',
          marginBottom:10,
          padding:10
        }}
      />
      </View>
      <TouchableOpacity 
      onPress={()=>{//untuk membuat tombol bisa di tekan/klik
        ambil();// funcion untuk ambil data yang sudah di input.
      }}>
      <View style={{
        justifyContent:'center',
        alignItems:'center'
        }}> 
        <Text style = {{fontWeight:'bold', 
          padding:10, 
          backgroundColor:'white', 
          textAlign:'center',
          width:80,
        }}>Input</Text>
      </View> 
     </TouchableOpacity>
     <View style={{
      justifyContent:'center',
      alignItems:'center'
     }}>

    <Text style = {{fontWeight:'bold', 
        justifyContent:'center',
        alignContent:'center',
        padding:10, 
        backgroundColor:'white', 
        textAlign:'center',
        width:80,
        marginTop:10}}>
      {nama}
    </Text>
     </View>
    </SafeAreaView>
    </ImageBackground>
  )
}

export default App